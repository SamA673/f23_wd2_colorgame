/* eslint-disable no-undef */
// eslint-disable-next-line strict, lines-around-directive
'use strict';

/**
 * @file A JavaScript file for sorting and storing a stats array (Saving / Loading / Clearing too!)
 * @author Sam A
 * @version 1.0
 * @date 2023-10-23
 */

// Stats Table Headings
const playerHeading = document.getElementById('player_heading');
const durationHeading = document.getElementById('duration_heading');
const clicksHeading = document.getElementById('clicks_heading');
const levelHeading = document.getElementById('level_heading');
const successHeading = document.getElementById('success_heading');
const sizeHeading = document.getElementById('size_heading');

// Stats Save / Load / Clear Buttons
const saveButton = document.getElementById('save_button');
const loadButton = document.getElementById('load_button');
const clearButton = document.getElementById('clear_button');

const levelOrder = ['easy', 'medium', 'difficult', 'brutal'];
let statsSorting = 0;
let sortedStatsArray;
let deletionClickCount = 0;
let deletionTimerSeconds = 0;
let deletionTimer;

/**
* This function clears the local storage of it's current
  contents and adds the current statsArray into it.
*/
function save() {
  localStorage.clear();
  localStorage.setItem('statsArray', JSON.stringify(statsArray));
}

/**
 * This function finds the statsArray item stored in localStorage (if it exists in the first place)
 * and redraws the table using said statsArray.
 */
function load() {
  const savedStatsArray = localStorage.getItem('statsArray');

  if (savedStatsArray) {
    // The 'statsArray' item exists in local storage
    statsArray = JSON.parse(savedStatsArray);
    addRows(statsArray);
  }
}

/**
 * This function deals with the confirmation on the clear all button.
 * After 3 seconds, the timer dies and resets the text to what it was.
 */
function deletionTimerAddSecond() {
  deletionTimerSeconds += 1;
  if (deletionTimerSeconds > 3) {
    clearButton.innerText = 'CLEAR ALL';
    clearInterval(deletionTimer);
    deletionClickCount = 0;
    deletionTimerSeconds = 0;
  }
}
/**
 * This function clears all data from localStorage.
 * After doing so, it clears the active statsArray so nothing is left in it.
 */
function clear() {
  deletionClickCount += 1;
  if (deletionClickCount === 2) {
    localStorage.clear();
    statsArray.length = 0;
    addRows(statsArray);
    clearButton.innerText = 'CLEAR ALL';
    deletionClickCount = 0;
  } else if (deletionClickCount < 2) {
    deletionTimer = setInterval(deletionTimerAddSecond, 1000);
    clearButton.innerText = 'CONFIRM DELETION';
  }
}

saveButton.addEventListener('click', save);
loadButton.addEventListener('click', load);
clearButton.addEventListener('click', clear);

/**
   * This following 6 sort function sort the table by Player(name), Duration, Clicks,
   * Levels, Success and Size. (Ascending, Descending, Default)
   * To do so, it:
   *                - Creates a deep copy stats array that will be edited and displayed.
   *                - Sets the stats headings accordingly to the sort version.
   *                - Finally, the cloned statsArray that has been
   *                  sorted is displayed on the stats table.
   *
   *                The sorting happens according to the following 3 conditions:
   *                  - If sorted already ascending, it sorts by descending.
   *                  - If sorted already descending, it goes back to default.
   *                  - If neither of the above 2 cases, it sorts by ascending order.
   *
   *                - Once sorted, statsSorting holds the value of the sort
   *                  according to the specific sort.
   *                Here are the sorting versions:
   *                    0   = DEFAULT
                        1   = NAMES ASCENDING     | 2 = NAMES DESCENDING
                        3   = DURATION ASCENDING  | 4 = DURATION DESCENDING
                        5   = CLICKS ASCENDING    | 6 = CLICKS DESCENDING
                        7   = LEVEL ASCENDING     | 8 = LEVEL DESCENDING
                        9   = SUCCESS ASC         | 10 = SUCCESS DESC
                        11  = SIZE ASC            | 12 = SIZE DESC

                    -  @see clearHeadings() : Clears all headings
*/

/**
 * This function sorts (by name) and displays the cloned array.
 */
function sortName() {
  sortedStatsArray = [...statsArray];
  clearHeadings();
  if (statsSorting === 1) { // already ascending
    statsSorting = 2;
    // Code to sort statsArray in descending order by name
    sortedStatsArray.sort((a, b) => b.player.localeCompare(a.player));
    playerHeading.innerText = 'Player (DESC)';
  } else if (statsSorting === 2) { // already descending
    statsSorting = 0;
    // Code to set statsArray to the cloned defaultStats array
    sortedStatsArray = [...statsArray];
    playerHeading.innerText = 'Player';
  } else {
    statsSorting = 1;
    // Code to sort statsArray in ascending order by name
    sortedStatsArray.sort((a, b) => a.player.localeCompare(b.player));
    playerHeading.innerText = 'Player (ASC)';
  }
  addRows(sortedStatsArray); // Regenerate the table based on sortedStatsArray
}

/**
* This function sorts (by duration) and displays the cloned array.
*/
function sortDuration() {
  sortedStatsArray = [...statsArray];
  clearHeadings();
  if (statsSorting === 3) { // already ascending
    statsSorting = 4;
    // Code to sort statsArray in descending order by durationS
    sortedStatsArray.sort((a, b) => b.durationS - a.durationS);
    durationHeading.innerText = 'Duration (DESC)';
  } else if (statsSorting === 4) { // already descending
    statsSorting = 0;
    // Code to set statsArray to the cloned defaultStats array
    sortedStatsArray = [...statsArray];
    durationHeading.innerText = 'Duration(s)';
  } else {
    statsSorting = 3;
    // Code to sort sortedStatsArray in ascending order by durationS
    sortedStatsArray.sort((a, b) => a.durationS - b.durationS);
    durationHeading.innerText = 'Duration (ASC)';
  }
  addRows(sortedStatsArray); // Regenerate the table based on sortedStatsArray
}

/**
* This function sorts (by clicks) and displays the cloned array.
*/
function sortClicks() {
  sortedStatsArray = [...statsArray];
  clearHeadings();
  if (statsSorting === 5) { // already ascending
    statsSorting = 6;
    // Code to sort statsArray in descending order by clicks
    sortedStatsArray.sort((a, b) => b.clicks - a.clicks);
    clicksHeading.innerText = 'Clicks (DESC)';
  } else if (statsSorting === 6) { // already descending
    statsSorting = 0;
    // Code to set statsArray to the cloned defaultStats array
    sortedStatsArray = [...statsArray];
    clicksHeading.innerText = 'Clicks';
  } else {
    statsSorting = 5;
    // Code to sort sortedStatsArray in ascending order by clicks
    sortedStatsArray.sort((a, b) => a.clicks - b.clicks);
    clicksHeading.innerText = 'Clicks(ASC)';
  }
  addRows(sortedStatsArray); // Regenerate the table based on sortedStatsArray
}

/**
* This function sorts (by success) and displays the cloned array.
*/
function sortSuccess() {
  sortedStatsArray = [...statsArray];
  clearHeadings();
  if (statsSorting === 9) { // already ascending
    statsSorting = 10;
    // Code to sort statsArray in descending order by success
    sortedStatsArray.sort((a, b) => b.success - a.success);
    successHeading.innerText = 'Success (DESC)';
  } else if (statsSorting === 10) { // already descending
    statsSorting = 0;
    // Code to set statsArray to the cloned defaultStats array
    sortedStatsArray = [...statsArray];
    successHeading.innerText = 'Success';
  } else {
    statsSorting = 9;
    // Code to sort sortedStatsArray in ascending order by success
    sortedStatsArray.sort((a, b) => a.success - b.success);
    successHeading.innerText = 'Success(ASC)';
  }
  addRows(sortedStatsArray); // Regenerate the table based on sortedStatsArray
}

/**
* This function sorts (by size) and displays the cloned array.
*/
function sortSize() {
  sortedStatsArray = [...statsArray];
  clearHeadings();
  if (statsSorting === 11) { // already ascending
    statsSorting = 12;
    // Code to sort statsArray in descending order by size
    sortedStatsArray.sort((a, b) => b.size - a.size);
    sizeHeading.innerText = 'Size (DESC)';
  } else if (statsSorting === 12) { // already descending
    statsSorting = 0;
    // Code to set statsArray to the cloned defaultStats array
    sortedStatsArray = [...statsArray];
    sizeHeading.innerText = 'Size';
  } else {
    statsSorting = 11;
    // Code to sort sortedStatsArray in ascending order by size
    sortedStatsArray.sort((a, b) => a.size - b.size);
    sizeHeading.innerText = 'Size(ASC)';
  }
  addRows(sortedStatsArray); // Regenerate the table based on sortedStatsArray
}

/**
* This function sorts (by level) and displays the cloned array.
*/
function sortLevel() {
  sortedStatsArray = [...statsArray];
  clearHeadings();

  if (statsSorting === 7) { // already ascending
    statsSorting = 8;
    // Code to sort statsArray in descending order by level
    sortedStatsArray.sort((a, b) => levelOrder.indexOf(b.level) - levelOrder.indexOf(a.level));
    levelHeading.innerText = 'Level (DESC)';
  } else if (statsSorting === 8) { // already descending
    statsSorting = 0;
    // Code to set statsArray to the cloned defaultStats array
    sortedStatsArray = [...statsArray];
    levelHeading.innerText = 'Level';
  } else {
    statsSorting = 7;
    // Code to sort sortedStatsArray in ascending order by level
    sortedStatsArray.sort((a, b) => levelOrder.indexOf(a.level) - levelOrder.indexOf(b.level));
    levelHeading.innerText = 'Level (ASC)';
  }

  addRows(sortedStatsArray); // Regenerate the table based on sortedStatsArray
}

playerHeading.addEventListener('click', sortName);
durationHeading.addEventListener('click', sortDuration);
clicksHeading.addEventListener('click', sortClicks);
levelHeading.addEventListener('click', sortLevel);
successHeading.addEventListener('click', sortSuccess);
sizeHeading.addEventListener('click', sortSize);
