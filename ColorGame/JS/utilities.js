/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-vars */
/**
 * @file a utilities file with helper methods like creating elements and disabling buttons
 * @author Adam Winter
 * @version 1.0
 * @date 11/2/2023
 */

/**
 *  @description
 * createMyElement -
 * . create a dynamic HTMLElement, set its properties
 * . and attach it to the parent (HTMLElement)
 * . The properties (rest parameter- check labs/notes):
 *      . must have at least id, type,
 *      . optionally text content, value, class name, min, max
 *
 * @param {HTMLElement} parent the object to append the new element to
 * @param {HTMLElement} elemTag the html tag of the new element being created
 * @param  {...any} properities additional properties for the element
 * @returns {HTMLElement} / return the created element
  */

function createMyElement(parent, elemTag, ...any) {
  const newElem = document.createElement(elemTag);
  if (any[0]) { newElem.id = any[0]; }
  if (any[1]) { newElem.type = any[1]; }
  if (any[2]) { newElem.className = any[2]; }
  if (any[3])(newElem.min = any[3]);
  if (any[4])(newElem.max = any[4]);
  if (any[5])(newElem.value = any[5]);
  if (any[6])(newElem.textContent = any[6]);

  parent.appendChild(newElem);
  return newElem;
}

class Sound {
  constructor(src) {
    this.sound = document.createElement('audio');
    this.sound.src = src;
    this.sound.setAttribute('preload', 'auto');
    this.sound.setAttribute('controls', 'none');
    this.sound.style.display = 'none';
    document.body.appendChild(this.sound);
    this.play = function () {
      this.sound.play();
    };
    this.stop = function () {
      this.sound.pause();
    };
  }
}
